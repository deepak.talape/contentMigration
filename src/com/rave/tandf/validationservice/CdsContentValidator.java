package com.rave.tandf.validationservice;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class CdsContentValidator {
	
public static final String VALIDATION_SITE_TEMP_FOLDERS = "Temp";
	
	public static final String MIX_ISBN_REGEX = "[0-9\\-\\ ]{13,17}";
	public static final Pattern MIX_ISBN_PATTERN = Pattern.compile(MIX_ISBN_REGEX);
	public static final String MIX_ISBN_FILE_NAME_REGEX = "^(" +MIX_ISBN_REGEX + ")(_.*)$";
    public static final Pattern MIX_ISBN_FILE_NAME_PATTERN = Pattern.compile(MIX_ISBN_FILE_NAME_REGEX);
    
	public static final String INGESTION_DATE_FORMAT = "MM/dd/yyyy HH:mm:ss";
	public static final String FOLDER_STRUCTURE_DATE_FORMAT = "yyyy/MM/dd";
	
	public static final String CMIS_FOLDER     = "CMIS_FOLDER";
	public static final String CMIS_DOCUMENT   = "cmis:document";
	public static final String PROPERTIES_FILE = "ICMSConnection.properties";
	
	public static final String REGEX_AUTHOR_CODE = RegexConstants.NUMERIC.getConstant();
	public static final String REGEX_BP_UNIQUE_REF_NO = RegexConstants.NUMERIC.getConstant();
	public static final String REGEX_PURCHASER_REF_NO = RegexConstants.NUMERIC.getConstant();
	//YYYY_MM_DD
	public static final String REGEX_STATEMENT_DATE =  "(((19|20)\\d\\d)_(0?[1-9]|[12][0-9]|3[01])_(0?[1-9]|1[012]))";
	public static final String REGEX_PERMISSION_REF = RegexConstants.NUMERIC.getConstant();
	
	public static final String REGEX_ISBN10 = new StringBuilder("(([\\d]{1})((?=(?:\\d*[\\-\\ ]){3}\\d*)[\\d\\ \\-]{11})([\\d]{1})|")
			.append("([\\d]{1})((?=(?:\\d*[\\-\\ ]){2}\\d*)[\\d\\ \\-]{10})([\\d]{1})|")
			.append("([\\d]{1})((?=(?:\\d*[\\-\\ ]){1}\\d*)[\\d\\ \\-]{9})([\\d]{1})|")
			.append("[\\d]{10})").toString();
	
	public static final String REGEX_ISBN13 = new StringBuilder("(([\\d]{1})((?=(?:\\d*[\\-\\ ]){4}\\d*)[\\d\\ \\-]{15})([\\d]{1})|")
			.append("([\\d]{1})((?=(?:\\d*[\\-\\ ]){3}\\d*)[\\d\\ \\-]{14})([\\d]{1})|")
			.append("([\\d]{1})((?=(?:\\d*[\\-\\ ]){2}\\d*)[\\d\\ \\-]{13})([\\d]{1})|")
			.append("([\\d]{1})((?=(?:\\d*[\\-\\ ]){1}\\d*)[\\d\\ \\-]{12})([\\d]{1})|")
			.append("[\\d]{13})").toString();
	
	
	/**
	 * The Enum RegexConstants.
	 */
	public enum RegexConstants {
		LEFT_BRACKET("("),
		RIGHT_BRACKET(")"),
		START_SENTENCE("^"),
		END_SENTENCE("$"),
		PIPE("|"),
		UNDERSCORE("_"),
		SELECT_ALL(".*"),
		NUMERIC("(\\d+)"),
		NUMERIC_OPTIONAL("(_\\d+)?"),
		FILE_EXTENSION(new StringBuilder("(\\.)").append("([^\\.]+)").toString())
		;
		
		private RegexConstants(final String constant){
			this.constant = constant;
		}
		
		private String constant;

		public String getConstant() {
			return constant;
		}
	}
	

	/**
	 * Creates the ISBN specific regex.
	 *
	 * @param key
	 *            the key
	 * @param isIsbn10Needed
	 *            the is isbn 10 needed
	 * @param isNumeric
	 *            the is numeric
	 * @return the string
	 */
	private static String createISBNSpecificRegex(final String key, final boolean isIsbn10Needed, final boolean isNumeric) {

		final StringBuilder builder = new StringBuilder(RegexConstants.START_SENTENCE.getConstant());

		if (isIsbn10Needed) {
			builder.append(RegexConstants.LEFT_BRACKET.getConstant()).append(REGEX_ISBN10).append(RegexConstants.PIPE.getConstant());
		}

		builder.append(REGEX_ISBN13);

		if (isIsbn10Needed) {
			builder.append(RegexConstants.RIGHT_BRACKET.getConstant());
		}

		builder.append(RegexConstants.LEFT_BRACKET.getConstant()).append(RegexConstants.UNDERSCORE.getConstant()).append(key).append(RegexConstants.RIGHT_BRACKET.getConstant());

		if (isNumeric) {
			builder.append(RegexConstants.NUMERIC_OPTIONAL.getConstant());
		}

		builder.append(RegexConstants.FILE_EXTENSION.getConstant()).append(RegexConstants.END_SENTENCE.getConstant());

		// LOG.debug("Inside createISBNSpecificRegex Key : {}, Regex : {}", key,
		// builder.toString());

		return builder.toString();
	}

	/**
	 * Creates the author code specific regex.
	 *
	 * @param key
	 *            the key
	 * @return the string
	 */
	private static String createAuthorCodeSpecificRegex(final String key) {
		final StringBuilder builder = createGenericRegex(key, REGEX_AUTHOR_CODE);
		// LOG.debug("Inside createAuthorCodeSpecificRegex Key : {}, Regex :
		// {}", key, builder.toString());
		return builder.toString();
	}

	/**
	 * Creates the BP unique ref no specific regex.
	 *
	 * @param key
	 *            the key
	 * @return the string
	 */
	private static String createBPUniqueRefNoSpecificRegex(final String key) {
		final StringBuilder builder = createGenericRegex(key, REGEX_BP_UNIQUE_REF_NO);
		// LOG.debug("Inside createBPUniqueRefNoSpecificRegex Key : {}, Regex :
		// {}", key, builder.toString());
		return builder.toString();
	}

	/**
	 * Creates the permission specific regex.
	 *
	 * @param key
	 *            the key
	 * @return the string
	 */
	private static String createPermissionSpecificRegex(final String key) {
		final StringBuilder builder = createGenericRegex(key, REGEX_PERMISSION_REF);
		// LOG.debug("Inside createPermissionSpecificRegex Key : {}, Regex :
		// {}", key, builder.toString());
		return builder.toString();
	}

	/**
	 * Creates the statement date specific regex.
	 *
	 * @param key1
	 *            the key 1
	 * @param key2
	 *            the key 2
	 * @return the string
	 */
	private static String createStatementDateSpecificRegex(final String key1, final String key2) {

		final StringBuilder builder = new StringBuilder(RegexConstants.START_SENTENCE.getConstant()).append(REGEX_PURCHASER_REF_NO)

				.append(RegexConstants.LEFT_BRACKET.getConstant()).append(RegexConstants.UNDERSCORE.getConstant()).append(key1).append(RegexConstants.UNDERSCORE.getConstant()).append(RegexConstants.RIGHT_BRACKET.getConstant())

				.append(RegexConstants.LEFT_BRACKET.getConstant()).append(REGEX_STATEMENT_DATE).append(RegexConstants.UNDERSCORE.getConstant()).append(REGEX_STATEMENT_DATE).append(RegexConstants.RIGHT_BRACKET.getConstant())

				.append(RegexConstants.LEFT_BRACKET.getConstant()).append(RegexConstants.UNDERSCORE.getConstant()).append(key2).append(RegexConstants.RIGHT_BRACKET.getConstant())

				.append(RegexConstants.FILE_EXTENSION.getConstant()).append(RegexConstants.END_SENTENCE.getConstant());

		// LOG.debug("Inside createStatementDateSpecificRegex Key1 : {}, Key2 :
		// {}, Regex : {}", key1, key2, builder.toString());

		return builder.toString();
	}

	/**
	 * Creates the generic regex.
	 *
	 * @param key
	 *            the key
	 * @param keyRegex
	 *            the author
	 * @return the string builder
	 */
	private static StringBuilder createGenericRegex(final String key, final String keyRegex) {
		final StringBuilder builder = new StringBuilder(RegexConstants.START_SENTENCE.getConstant());

		builder.append(keyRegex).append(RegexConstants.LEFT_BRACKET.getConstant()).append(RegexConstants.UNDERSCORE.getConstant()).append(key).append(RegexConstants.RIGHT_BRACKET.getConstant()).append(RegexConstants.FILE_EXTENSION.getConstant()).append(RegexConstants.END_SENTENCE.getConstant());
		return builder;
	}

	public static boolean validate(final String fileName) {

		List<String> patterns = new ArrayList<>();
		patterns.add(createISBNSpecificRegex("Contract", true, false));
		patterns.add(createISBNSpecificRegex("Addendum", true, false));
		patterns.add(createISBNSpecificRegex("Royalty_Correspondence", true, false));

		patterns.add(createAuthorCodeSpecificRegex("Author_Correspondence"));
		patterns.add(createAuthorCodeSpecificRegex("BankDetails"));
		patterns.add(createAuthorCodeSpecificRegex("Probate"));

		patterns.add(createBPUniqueRefNoSpecificRegex("SR\\-Licence"));
		patterns.add(createBPUniqueRefNoSpecificRegex("SR\\-Licence_Correspondence"));
		patterns.add(createBPUniqueRefNoSpecificRegex("SR\\-Cancellation"));
		patterns.add(createBPUniqueRefNoSpecificRegex("SR\\-Reversion"));

		patterns.add(createStatementDateSpecificRegex("SR\\-Statement", "Publisher"));

		patterns.add(createPermissionSpecificRegex("SR\\-Permission_Correspondence"));
		patterns.add(createPermissionSpecificRegex("SR\\-Permission"));

		return patterns.stream().filter(regex -> fileName.matches(regex)).findAny().orElse(null) != null;
	}

}

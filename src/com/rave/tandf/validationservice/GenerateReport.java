package com.rave.tandf.validationservice;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class GenerateReport {
	
	public static HSSFWorkbook generateValidationReport(int documentCount, String fileName, List<String> path, HSSFWorkbook workbook){
	String invalidFilesReport = "D:/CDSMetadataExcelSheet.xls";
		
	//HSSFWorkbook workbook=null;
	HSSFSheet sheet=null;
		if(documentCount==1){
			workbook = new HSSFWorkbook();
			sheet = workbook.createSheet("FirstSheet");
			HSSFRow rowhead = sheet.createRow((short) 0);
			rowhead.createCell(0).setCellValue("No.");
			rowhead.createCell(1).setCellValue("File Name");
			rowhead.createCell(2).setCellValue("File Path");
		}
		
	/*	 int rows=sheet.getLastRowNum();		
		if(rows!=0){
		sheet.shiftRows(2,rows,1);
			}	*/
		
		HSSFRow row = workbook.getSheet("FirstSheet").createRow((short) documentCount);
		row.createCell(0).setCellValue(documentCount);
		row.createCell(1).setCellValue(fileName);
		row.createCell(2).setCellValue(path.toString());
		
		FileOutputStream fileOut;
		try {
			fileOut = new FileOutputStream(invalidFilesReport);
			workbook.write(fileOut);
			fileOut.close();
			//System.out.println(documentCount +"Rows Added");
			System.out.println(documentCount);
		} catch (FileNotFoundException e) {
			System.out.println("File Not found Exception");
		} catch (IOException e) {
			System.out.println("Exception Occured");
		}
		return workbook;
	}

}

package com.rave.tandf.migration;

import com.rave.tandf.retriveservice.GetContentsByType;
import com.rave.tandf.utils.ContentTypeProperties;

public class MigrationMainClass {

	public static void main(String[] args) {		
		long documentCount=GetContentsByType.startMigration(ContentTypeProperties.MainContractDocuments);
		System.out.println("Document Count=" + documentCount);
	}

}

package com.rave.tandf.retriveservice;

import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.OperationContext;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.apache.chemistry.opencmis.client.runtime.OperationContextImpl;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.rave.tandf.transferservice.SendContentToTargetRepository;
import com.rave.tandf.utils.GetAlfrescoSession;
import com.rave.tandf.validationservice.CdsContentValidator;
import com.rave.tandf.validationservice.GenerateReport;

public class GetContentsByType {

	public static long startMigration(String cdsContentType) {
		CDSMetadataVO metadataVo = new CDSMetadataVO();
		Document doc=null;
		int documentCount = 0;
		int PAGE_NUM = 0;
		final int PAGE_SIZE = 100;	
		String query = "SELECT * FROM "+cdsContentType;
		//OperationContext oc = GetAlfrescoSession.getSourceRepositorysession().createOperationContext();
		OperationContext oc = new OperationContextImpl();
		oc.setMaxItemsPerPage(10000);
		// PAGE_NUM ISSUE ??
		ItemIterable<QueryResult> resultList = null;
		HSSFWorkbook workbook=null;	
		do {
			
			resultList = GetAlfrescoSession.getSourceRepositorysession().query(query, false, oc)
			.skipTo(PAGE_NUM * PAGE_SIZE).getPage(PAGE_SIZE);
			
	System.out.println("Total items:" + resultList.getTotalNumItems());
	System.out.println("Page items:" + resultList.getPageNumItems());
	
		for (QueryResult qr : resultList) {
			documentCount++;

			String idDocument = qr.getPropertyByQueryName("cmis:objectId").getFirstValue().toString();
			doc = (Document) GetAlfrescoSession.getSourceRepositorysession().getObject(idDocument);

				if (true == CdsContentValidator.validate(doc.getName())) {
				System.out.println("Valid");
				// Alfresco OOTB properties
				metadataVo.setFileName(doc.getName());
				metadataVo.setCreator(doc.getCreatedBy());
				metadataVo.setCreationDate(doc.getCreationDate());
				metadataVo.setLastModification(doc.getLastModificationDate());
				// All CDS AspectSpecific properties
				metadataVo.setISBN(doc.getPropertyValue("crandrc:ISBN"));
				metadataVo.setBookAuthor(doc.getPropertyValue("crandrc:bookAuthor"));
				metadataVo.setContractNumber(doc.getPropertyValue("crandrc:contractNumber"));
				metadataVo.setAssociatedISBNs(doc.getPropertyValue("crandrc:associatedISBNs"));
				metadataVo.setCreditorNumber(doc.getPropertyValue("crandrc:creditorNumber"));
				metadataVo.setOcrSucceeded(doc.getPropertyValue("crandrc:ocrSucceeded"));
				metadataVo.setOcrText(doc.getPropertyValue("crandrc:ocrText"));

				System.out.println("AssociatedISBN=" + doc.getPropertyValue("crandrc:associatedISBNs"));
				
				
				System.out.println("ISBN=" + metadataVo.getISBN());
				System.out.println("File Name=" + metadataVo.getFileName());
				System.out.println("Creator=" + metadataVo.getCreator());
				System.out.println("Book Author=" + metadataVo.getBookAuthor());
				System.out.println("Creditor Number=" + metadataVo.getCreditorNumber());
			
				
				
				
				
				if( null != doc.getContentStream()){
							//SendContentToTargetRepository.transfer(doc.getContentStream(),metadataVo);
				}else {					
					System.out.println("Document Having null content stream");
				}

			} else {
				workbook =  GenerateReport.generateValidationReport(documentCount, doc.getName(), doc.getPaths(),workbook);
				//System.out.println("Invalid");
			}

		} 
		++PAGE_NUM;
		}while (resultList.getHasMoreItems()) ;
		System.out.println(resultList.getHasMoreItems());
		return documentCount;

	}

}

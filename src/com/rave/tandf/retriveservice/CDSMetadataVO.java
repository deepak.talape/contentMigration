package com.rave.tandf.retriveservice;

import java.util.GregorianCalendar;
import java.util.List;

public class CDSMetadataVO {

	private String fileName;
	private String creator;
	private GregorianCalendar creationDate;
	private GregorianCalendar lastModification;
	private String ISBN;
	private String bookAuthor;
	private String contractNumber;
	private List<?> associatedISBNs;
	private String creditorNumber;
	private boolean ocrSucceeded;
	private String ocrText;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public GregorianCalendar getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(GregorianCalendar creationDate) {
		this.creationDate = creationDate;
	}

	public GregorianCalendar getLastModification() {
		return lastModification;
	}

	public void setLastModification(GregorianCalendar lastModification) {
		this.lastModification = lastModification;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

	public String getBookAuthor() {
		return bookAuthor;
	}

	public void setBookAuthor(String bookAuthor) {
		this.bookAuthor = bookAuthor;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public List<?> getAssociatedISBNs() {
		return associatedISBNs;
	}

	public void setAssociatedISBNs(List<?> associatedISBNs) {
		this.associatedISBNs = associatedISBNs;
	}

	public String getCreditorNumber() {
		return creditorNumber;
	}

	public void setCreditorNumber(String creditorNumber) {
		this.creditorNumber = creditorNumber;
	}

	public boolean isOcrSucceeded() {
		return ocrSucceeded;
	}

	public void setOcrSucceeded(boolean ocrSucceeded) {
		this.ocrSucceeded = ocrSucceeded;
	}

	public String getOcrText() {
		return ocrText;
	}

	public void setOcrText(String ocrText) {
		this.ocrText = ocrText;
	}

}

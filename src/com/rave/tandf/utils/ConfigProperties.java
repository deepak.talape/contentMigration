
package com.rave.tandf.utils;

import java.util.ResourceBundle;

public class ConfigProperties {
	    static ResourceBundle bundle = ResourceBundle.getBundle("app");

	    public static String sourceRepositoryUrl = bundle.getString("sourceRepositoryUrl"); 
	    public static String sourceUsername = bundle.getString("sourceUsername"); 
	    public static String sourcePassword = bundle.getString("sourcePassword"); 
	    
	    public static String targetRepositoryUrl = bundle.getString("targetRepositoryUrl"); 
	    public static String targetUsername = bundle.getString("targetUsername"); 
	    public static String targetPassword = bundle.getString("targetPassword"); 	    
	  
}

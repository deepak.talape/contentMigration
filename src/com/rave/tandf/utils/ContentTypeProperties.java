package com.rave.tandf.utils;

import java.util.ResourceBundle;

public class ContentTypeProperties {
	static ResourceBundle bundle = ResourceBundle.getBundle("CdsContentTypes");

    public static String AllTypeDocuments = bundle.getString("AllTypeDocuments"); 
    public static String MainContractDocuments = bundle.getString("MainContractDocuments"); 
    public static String SubRightsDocuments = bundle.getString("SubRightsDocuments");     
    public static String RoyaltiesDocument = bundle.getString("RoyaltiesDocument"); 
    public static String CorrespondenceDocuments = bundle.getString("CorrespondenceDocuments"); 
    public static String AuthorAccountFormsDocuments = bundle.getString("AuthorAccountFormsDocuments"); 	
  
}

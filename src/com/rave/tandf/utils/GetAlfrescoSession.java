package com.rave.tandf.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.Repository;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.bindings.CmisBindingFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.enums.BindingType;

public class GetAlfrescoSession {

	static Session sourceSession = null;
	static Session targetSession = null;
	public static Session getSourceRepositorysession() {



		if (sourceSession == null) {
			//ADD LOGGER
			//SINGLE SESSION FOR ENTIRE APP ***
			System.out.println("Inside session.....");
			// default factory implementation
			SessionFactory factory = SessionFactoryImpl.newInstance();
			Map<String, String> parameter = new HashMap<String, String>();

			// user credentials
			parameter.put(SessionParameter.USER, ConfigProperties.sourceUsername);
			parameter.put(SessionParameter.PASSWORD, ConfigProperties.sourcePassword);

			// Uncomment for Atom Pub binding
			parameter.put(SessionParameter.ATOMPUB_URL, ConfigProperties.sourceRepositoryUrl);

			// Uncomment for Atom Pub binding
			parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());
			parameter.put(SessionParameter.AUTHENTICATION_PROVIDER_CLASS,
					CmisBindingFactory.NTLM_AUTHENTICATION_PROVIDER);

			List<Repository> repositories = factory.getRepositories(parameter);
			sourceSession = repositories.get(0).createSession();
		}
		return sourceSession;

	}

	public static Session getTargetRepositorysession() {
		
		if (targetSession == null) {
			//System.out.println("Getting Target Alfresco Session");

			Map<String, String> parameter = new HashMap<String, String>();

			// Set the user credentials
			parameter.put(SessionParameter.USER, ConfigProperties.targetUsername);
			parameter.put(SessionParameter.PASSWORD, ConfigProperties.targetPassword);

			// Specify the connection settings
			parameter.put(SessionParameter.ATOMPUB_URL, ConfigProperties.targetRepositoryUrl);
			parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());

			// Set the alfresco object factory
			parameter.put(SessionParameter.OBJECT_FACTORY_CLASS,
					"org.alfresco.cmis.client.impl.AlfrescoObjectFactoryImpl");

			// Create a session
			SessionFactory factory = SessionFactoryImpl.newInstance();
			targetSession = factory.getRepositories(parameter).get(0).createSession();
		}
		return targetSession;
	}

}

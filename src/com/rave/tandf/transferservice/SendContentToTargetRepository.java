package com.rave.tandf.transferservice;

import java.io.File;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.activation.MimetypesFileTypeMap;

import org.alfresco.cmis.client.AlfrescoDocument;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Policy;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.data.Ace;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;
import org.apache.commons.io.FilenameUtils;

import com.rave.tandf.retriveservice.CDSMetadataVO;
import com.rave.tandf.utils.GetAlfrescoSession;

public class SendContentToTargetRepository {

	public static void transfer(ContentStream cs, CDSMetadataVO metadataVo1) {

		InputStream is = cs.getStream();
		/*if (cs != null) {
			is = cs.getStream();
		}*/

		//CDSMetadataVO metadataVo1 = new CDSMetadataVO();
		Session sessionB = GetAlfrescoSession.getTargetRepositorysession();
		String fileName = metadataVo1.getFileName();

		// GET THE DESTINATION FOLDER THAT YOU WILL WORK WITH

		String path = "/Sites/test/documentLibrary/";
		Folder ingestionFolder = (Folder) sessionB.getObjectByPath(path);

		File newfile = new File(fileName);
		Map<String, Object> newFileProperties = new HashMap<>();
		newFileProperties.put(PropertyIds.OBJECT_TYPE_ID, BaseTypeId.CMIS_DOCUMENT.value());
		System.out.println("File name :: " + fileName);
		newFileProperties.put(PropertyIds.NAME, fileName);

		List<Ace> addAces = new LinkedList<>();
		List<Ace> removeAces = new LinkedList<>();
		List<Policy> policies = new LinkedList<>();
	//	String extension = FilenameUtils.getExtension(fileName);
		ContentStream contentStream = new ContentStreamImpl("content." + FilenameUtils.getExtension(fileName),
				BigInteger.valueOf(fileName.length()), new MimetypesFileTypeMap().getContentType(newfile), (is));

		Document newDocument = (Document) getObject(sessionB, ingestionFolder, fileName);

		if (newDocument == null) {
			Document dc = ingestionFolder.createDocument(newFileProperties, contentStream, VersioningState.MAJOR,
					policies, addAces, removeAces, sessionB.getDefaultContext());

			AlfrescoDocument alfDoc = (AlfrescoDocument) dc;
			alfDoc.addAspect("P:tafcms:oldProperties");

			if (alfDoc.hasAspect("P:tafcms:oldProperties")) {
				HashMap<String, Object> aspectProperties = new HashMap<String, Object>();

				aspectProperties.put("tafcms:fileName", fileName);
				aspectProperties.put("tafcms:documentOwner", metadataVo1.getCreator());
				aspectProperties.put("tafcms:creationDate", metadataVo1.getCreationDate());
				aspectProperties.put("tafcms:modificationDate", metadataVo1.getLastModification());

				aspectProperties.put("tafcms:ISBN", metadataVo1.getISBN());
				aspectProperties.put("tafcms:bookAuthor", metadataVo1.getBookAuthor());
				aspectProperties.put("tafcms:contractNumber", metadataVo1.getContractNumber());

				System.out.println("Associated ISBN=" + metadataVo1.getAssociatedISBNs());
				aspectProperties.put("tafcms:associatedISBNs", metadataVo1.getAssociatedISBNs());

				aspectProperties.put("tafcms:creditorNumber", metadataVo1.getCreditorNumber());
				aspectProperties.put("tafcms:ocrSucceeded", metadataVo1.isOcrSucceeded());
				aspectProperties.put("tafcms:ocrText", metadataVo1.getOcrText());

				alfDoc.updateProperties(aspectProperties);

			}

			System.out.println("Document Created=" + dc.getName());

		}

	}

	private static CmisObject getObject(Session session, Folder parentFolder, String objectName) {
		CmisObject object = null;

		try {
			String path2Object = parentFolder.getPath();
			if (!path2Object.endsWith("/")) {
				path2Object += "/";
			}
			path2Object += objectName;
			System.out.println("path_2_Object :: " + path2Object);
			object = session.getObjectByPath(path2Object);
		} catch (CmisObjectNotFoundException nfe0) {
			// Nothing to do, object does not exist
		}

		return object;
	}

}
